package com.tsc.skuschenko.tm.api.repository.dto;

import com.tsc.skuschenko.tm.dto.ProjectDTO;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public interface IProjectDTORepository
        extends IAbstractDTORepository<ProjectDTO> {

    void clear(@NotNull String userId);

    void clearAllProjects();

    @Nullable List<ProjectDTO> findAll();

    @Nullable List<ProjectDTO> findAllWithUserId(@NotNull String userId);

    @Nullable ProjectDTO findById(@NotNull String id);

    @Nullable
    ProjectDTO findOneById(@NotNull String userId, @NotNull String id);

    @Nullable
    ProjectDTO findOneByIndex(@NotNull String userId, @NotNull Integer index);

    @Nullable
    ProjectDTO findOneByName(@NotNull String userId, @NotNull String name);

    void removeOneById(@NotNull String userId, @NotNull String id);

}