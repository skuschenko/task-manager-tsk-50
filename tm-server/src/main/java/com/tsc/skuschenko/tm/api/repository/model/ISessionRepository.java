package com.tsc.skuschenko.tm.api.repository.model;

import com.tsc.skuschenko.tm.model.Session;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public interface ISessionRepository
        extends IAbstractRepository<Session> {

    void add(@NotNull Session session);

    @Nullable List<Session> findAll(@NotNull String userId);

    @Nullable
    Session getReference(@NotNull String id);

    @Nullable
    Session findSessionById(@NotNull Session session);

    void removeSessionById(@NotNull  Session session);

}
