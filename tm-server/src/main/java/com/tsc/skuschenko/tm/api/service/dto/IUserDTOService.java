package com.tsc.skuschenko.tm.api.service.dto;

import com.tsc.skuschenko.tm.dto.UserDTO;
import com.tsc.skuschenko.tm.enumerated.Role;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public interface IUserDTOService {

    void addAll(@Nullable List<UserDTO> users);

    void clear();

    @NotNull
    UserDTO create(@Nullable String login, @Nullable String password);

    @NotNull
    UserDTO create(
            @Nullable String login, @Nullable String password,
            @Nullable String email
    );

    @NotNull
    UserDTO create(
            @Nullable String login, @Nullable String password,
            @Nullable Role role
    );

    @Nullable
    @SneakyThrows
    List<UserDTO> findAll();

    @Nullable
    UserDTO findByEmail(@Nullable String email);

    @Nullable
    UserDTO findById(@Nullable String email);

    @Nullable
    UserDTO findByLogin(@Nullable String login);

    boolean isEmailExist(@Nullable String login);

    boolean isLoginExist(@Nullable String login);

    @NotNull
    UserDTO lockUserByLogin(@Nullable String login);

    @Nullable
    UserDTO removeByLogin(@Nullable String login);

    @NotNull
    UserDTO setPassword(@Nullable String userId, @Nullable String password);

    @NotNull
    UserDTO unlockUserByLogin(@Nullable String login);

    @NotNull
    UserDTO updateUser(
            @Nullable String userId, @Nullable String firstName,
            @Nullable String lastName, @Nullable String middleName
    );

}
