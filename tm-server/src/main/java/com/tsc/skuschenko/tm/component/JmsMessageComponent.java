package com.tsc.skuschenko.tm.component;

import com.tsc.skuschenko.tm.api.service.IBroadcastService;
import com.tsc.skuschenko.tm.service.BroadcastService;
import lombok.Getter;
import lombok.SneakyThrows;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.jetbrains.annotations.NotNull;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.JMSException;

@Getter
public class JmsMessageComponent {

    @NotNull
    public static JmsMessageComponent instance;

    @NotNull
    final IBroadcastService broadcastService = new BroadcastService();

    @NotNull
    private Connection connection;

    @NotNull
    private ConnectionFactory connectionFactory;

    public JmsMessageComponent() {
        init();
        instance = this;
    }

    @NotNull
    public static JmsMessageComponent getInstance() {
        return instance;
    }

    @SneakyThrows
    private void init() {
        connectionFactory = new ActiveMQConnectionFactory(
                ActiveMQConnectionFactory.DEFAULT_BROKER_URL
        );
        connection = connectionFactory.createConnection();
    }

    public void run() throws JMSException {
        connection.start();
    }

    public void shutdown() throws JMSException {
        connection.close();
        broadcastService.shutdown();
    }

}
