package com.tsc.skuschenko.tm.bootstrap;

import com.tsc.skuschenko.tm.api.ILoggingService;
import com.tsc.skuschenko.tm.api.IPropertyService;
import com.tsc.skuschenko.tm.service.LoggingService;
import com.tsc.skuschenko.tm.service.PropertyService;
import org.jetbrains.annotations.NotNull;

import javax.jms.JMSException;
import java.util.List;

public final class Bootstrap {

    public void run() {
        @NotNull final ILoggingService loggingService =
                new LoggingService();
        @NotNull final IPropertyService propertyService = new PropertyService();
        @NotNull final List<String> entities = propertyService.getLogEntities();
        entities.forEach(item -> {
            try {
                loggingService.createBroadcastConsumer(item);
            } catch (JMSException e) {
                e.fillInStackTrace();
            }
        });
    }

}
