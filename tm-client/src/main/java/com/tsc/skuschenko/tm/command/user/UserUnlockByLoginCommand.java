package com.tsc.skuschenko.tm.command.user;


import com.tsc.skuschenko.tm.endpoint.Session;
import com.tsc.skuschenko.tm.exception.entity.session.AccessForbiddenException;
import com.tsc.skuschenko.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Optional;

public final class UserUnlockByLoginCommand extends AbstractUserCommand {

    @NotNull
    private static final String DESCRIPTION = "unlock user by login";

    @NotNull
    private static final String NAME = "unlock-user-by-login";

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        showOperationInfo(NAME);
        @Nullable final Session session = serviceLocator.getSession();
        Optional.ofNullable(session).orElseThrow(AccessForbiddenException::new);
        showParameterInfo("login");
        @NotNull final String login = TerminalUtil.nextLine();
        serviceLocator.getUserEndpoint().unlockUserByLogin(session, login);
    }

    @NotNull
    @Override
    public String name() {
        return NAME;
    }

    @NotNull
    @Override
    public String[] roles() {
        return new String[]{"Administrator"};
    }

}
